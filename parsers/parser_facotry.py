from parsers.sabre_parser import SabreParser


def parser_factory(provider):
    parser_mapping = {
        "sabre": SabreParser.__class__
    }
    return parser_mapping[provider]

