
from parsers.base_parser import BaseParser


class SabreParser(BaseParser):
    def __init__(self):
        pass

    def get_request(self, input):
        raise NotImplementedError

    def parse_response(self, api_response):
        raise NotImplementedError

    def get_proider_target_host(self):
        raise NotImplementedError
