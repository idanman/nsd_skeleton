import settings




class Base:
    def __init__(self):
        # logger
        pass


class Context(Base):
    def __init__(self, arguments):
        self.arguments = arguments
        self.settings = None
        self.load_settings(environment=arguments.environment)
    pass

    def load_settings(self, environment):
        self.settings = settings.get_configuration_by_environment(environment=environment)


