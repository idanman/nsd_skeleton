import eventlet
import argparse
import logging
import daiquiri
import datetime

from common import Context
from parsers.parser_facotry import parser_factory

eventlet.monkey_patch()


class NSD:
    def __init__(self):
        self.argparser = argparse.ArgumentParser(description='None scan data workers monolith')
        self.prepare_arguments()
        self.arguments = self.argparser.parse_args()
        self.context = Context(self.arguments)
        self.logger = self.initialize_logger()
        self.queue_client = self.get_queue_client()

    def prepare_arguments(self):
        self.argparser.add_argument('-env', '--environment', help="Set the environment to use", dest='environment',
                                    default="development")
        self.argparser.add_argument('-V', '--verbosity', action="count", default=0, help="increase output verbosity",
                                    dest='verbosity')

    def initialize_logger(self):
        log_level = logging.INFO
        if self.arguments.verbosity > 1:
            log_level = logging.DEBUG
        # Log file output
        daiquiri.setup(level=log_level,
                       outputs=(
                           ('stdout', 'stderr'),
                           # daiquiri.output.File('nsd_errors.log', level=logging.ERROR),
                           # daiquiri.output.TimedRotatingFile(
                           #     'nsd.log',
                           #     level=logging.DEBUG,
                           #     interval=datetime.timedelta(days=1))
                           )
                       )
        return daiquiri.getLogger(__name__)

    def get_queue_client(self):
        pass

    def create_workers(self, number_of_threads):

        def get_provider(message):
            pass

        def worker(internal_queue):
            message = internal_queue.get()
            provider = get_provider(message=message)
            parser = parser_factory(provider)()

        pass


    def graceful_shutdown(self):
        pass

    def run(self):
        self.create_workers(number_of_threads = self.settings.number_of_threads)

        while True:
            try:
                # Read messages from SQS
                message = self.queue_client.fetch_message()
                self.input_processor(message)
            except KeyboardInterrupt :
                self.graceful_shutdown()
            except Exception as e:
                self.logger.exception("An unexcpected exception occured!\n{exception}".format(exception=str(e)))
                self.graceful_shutdown()


if __name__ == '__main__':
    NSD().run()



