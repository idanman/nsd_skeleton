

configuration = {
    "development": {
        "number_of_workers": 1,
    },
    "production": {
        "number_of_workers": 200,
    }
}


def worker_factory(provider):
    mapping = {"sabre": Sabre.__class__}
    return mapping.get(provider)


def get_configuration_by_environment(environment="development"):
    return configuration.get(environment)


# def get_settings_by_key_and_environment(setting_key, environment="development", default=None, ):
#     return configuration.get(environment).get(setting_key, default=default)
